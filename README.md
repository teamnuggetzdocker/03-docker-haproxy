# Docker Demo - HAProxy

- Run this command in the docker-haproxy/haproxy git repo directory

	docker build -t nttdatalab/haproxy .

- Run this command in the docker-haproxy/haproxy-project-specific git repo directory

	docker build -t nttdatalab/haproxy-project-specific .
	
- Run this command

	docker run -d --name nuggetzbanktransactions --net nttdatalabnet nttdatalab/nuggetzbank-transactions

- Run this command
	
	docker run -d --name nuggetzbankaccounts1 --net nttdatalabnet nttdatalab/nuggetzbank-accounts
	
- Run this command
	
	docker run -d --name nuggetzbankaccounts2 --net nttdatalabnet nttdatalab/nuggetzbank-accounts	
	
- Run this command

	docker run -d -p 8080:8080 --name haproxyprojectspecific --net nttdatalabnet nttdatalab/haproxy-project-specific

- Run this command

	curl -i http://192.168.99.100:8080/nuggetzbank-accounts/resources/accounts
	
You should see a response similar to:

	HTTP/1.1 200 OK
	Connection: close
	X-Powered-By: Undertow/1
	Server: WildFly/10
	Content-Type: application/octet-stream
	Content-Length: 48
	Date: Wed, 20 Sep 2017 08:00:21 GMT
	an account response plus: a transaction response


	